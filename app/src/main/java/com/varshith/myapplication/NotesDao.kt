package com.varshith.myapplication

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.Query

@Dao
interface NotesDao
{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    //while we perform insert operation the data with same is ignored

     fun insert(note:Note)
    @Update
     fun update(note: Note)
    @Delete
     fun delete(note:Note)
    @Query("Select * from notesTable order by id ASC")
    fun getAllNotes():LiveData<List<Note>>
    @Query("SELECT * FROM notesTable WHERE Title LIKE :searchQuery ")
    fun searchDatabase(searchQuery: String):LiveData<List<Note>>


}
